# AO Scripts
The subfolder scripts is intended to use as-is inside the scripts folder of Anarchy Online. You can find this folder by opening Anarchy Online -> settings -> GUI -> Under folders, click Open on the same line as Scripts.

Typically a subfolder of "%localappdata%\\Funcom\\Anarchy Online"

## Autohotkey scripts
These scripts have .ahk ending and typically require autohotkey v2 to run. You can download it [here](https://www.autohotkey.com/)

- automatehotkeys.ahk: Offers a way to automatically use some keys every n seconds.

## Bash scripts
I write these scripts to be used in wsl, and they have .sh extention.

- backup-ao.sh: Is intended to backup the prefs folder, avoiding the chat logs in case they grow too large (and I have never felt any desire to recover them if they get lost)
