# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 04:08:54 2024

@author: ketil
"""
import textwrap
import re

def yellow(text: str):
    '''Wraps text in colourcode that makes it yellow'''
    #return f'<u>{text}</u>'
    return f"<font color='#FFFF00'>{text}</font>"

def command(c: str, text: str):
    '''
    Makes a clickable link for anarchy online. Only works when used inside a textbox.

    Parameters
    ----------
    c : str
        The command you type in the chat with / removed. I.e. if you type
        /camp in game, you want c='camp'
    text : str
        The name you give your clickable link

    Returns
    -------
    str
        AO HTML code for a clickable link in Anarchy Online

    '''
    return f"<a href='chatcmd:///{c}'>{text}</a>"

def make_textbox(contents: str, title: str):
    '''
    Makes a textbox Anarchy Online.

    Parameters
    ----------
    contents : str
        This is the AO html that go inside the textbox
    title : str
        This is the title of the link that opens the textbox

    Returns
    -------
    str
        AO HTML code that contains both the textbox and the text that is written in chat.

    '''
    return f'<a href="text://{contents}\">{title}</a>'

def convert_custom_format(textbox_contents: str, textbox_title: str, self_only=True):
    '''
    Converts human readable text to AO html.

    Parameters
    ----------
    textbox_contents : str
        The human readable description of what contents should go into the textbox
        
        A line containing the following
            [org ranks|Check the ranks of your org]
        
        will be converted to a clickable link where the text say
            Check the ranks of your org
        and if you click it, you will run the command /org ranks
        
        A line containing the following:
            Use !loot clear! to clear loot
        will be converted into AO HTML where "!loot clear" is written in yellow,
        while the other text on the line remains unaltered. The final exclamation mark
        is removed from the resulting AO HTML and will not be shown in the game.
        
    textbox_title : str
        The title of the clickable link that opens the textbox
    self_only : TYPE, optional
        If True, prefix the final command with /text so only you can see it.
        If False, the command will paste into the chat you type it in.

    Returns
    -------
    html : TYPE
        AO HTML for a script.

    '''
    # Define regular expressions for matching custom patterns
    yellow_pattern = r'#([^#]+)#'
    command_pattern = r'\[(.*?)\|(.*?)\]'

    # Replace custom patterns with HTML
    textbox_contents = re.sub(yellow_pattern,
                              lambda match: yellow(match.group(1)),
                              textbox_contents)
    textbox_contents = re.sub(command_pattern, 
                              lambda match: command(match.group(1), 
                                                    match.group(2)),
                              textbox_contents)
    
    textbox_contents = textwrap.dedent(textbox_contents).strip()
    html = make_textbox(textbox_contents, textbox_title)
    
    if self_only:
        html = '/text ' + html
    
    return html.replace('\n', '<br>')

def print_results(html):
    print('Check if this looks reasonable:')
    print(html.replace('<br>', '\n'), end='\n\n')
    
    print('Copy this into the script')
    print(html)


def bs():
    # This particular script is WIP, how shows how to use it.
    textbox_contents = """
        Guide to Battlestations:
        
        Check for Arbiter's Presence:
        
            Use #!icc# in org chat or click the link to check if the Arbiter is in the holodeck. Skip to the next step if not.
            If the Arbiter is present, go to [/waypoint 3245.6 939.8 655|waypoint].
            Speak with Arbitration Drone and Arbiter Vincenzo for quests upstairs.
            
                If assigned the civil disobedience quest, #/duel# a player after asking first. Use #/v# for vicinity chat.
                Strangers prefer to be asked at Unicorn Defence Hub, not elsewhere.
        
        Battlestations:
        
            Get the quest at [/waypoint 683.3 584.3 800|the bar]. Find John in the back to the right.
            Head to the grid, then Unicorn Defence Hub.
            Sign up after choosing a side.
            Avoid going #/afk# or using auto-afk in settings.
    """
    textbox_title =  'Guide for battlestations'
    return convert_custom_format(textbox_contents, textbox_title, self_only=True)

if __name__ == '__main__':
    html = bs()
    print_results(html)
