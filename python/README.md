# ao_itemsearch
This is an early app for searching the ao database for useful items. I've put the output of an example run into the  ao_itemsearch.txt.

I use AO Item Assistant + 2019 Edition to make my database, but other item assistants may or may not work. 

All configuration is intended to be done through `ao.yaml` which may or may not make the path for the database configurable. Sqlite doesn't like multiple apps talking to the same database so I recommend making a copy from `%localappdata%\Hallucina Software\AOIAPlus`. The file `aoitems.db` doesn't contain any important info about your characters, but is directly imported from the game. You don't need to worry about messing it up, because aoia can regenerate it for you.

However, you probably want to backup your `ItemAssistant.db` from time to time. If you lose it, you have to open your backpacks again to repopulate the database. I don't intend to use it directly. If I make a script that use it, then I plan to work on a copy, rather than the the db that belongs to aoia.

`ao.yaml` isn't fully populated yet. I've not seen proper documentation for the database format, so I've reversed engineered it, but so far I add what I want to use as I go, rather than add it all straight away.
