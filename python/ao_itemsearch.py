# -*- coding: utf-8 -*-
"""
Created on Mon May 22 15:15:46 2023

@author: ketil
"""
import enum
import sqlite3
import yaml


def get_key(d, v):
    '''For a dictionary structure, get k such that d[k] = v'''
    for k, v2 in d.items():
        if v == v2:
            return k
    raise ValueError(f'No key found for value {v}')


def get_nonzero_bit_indices(num):
    return [i for i, j in enumerate('{:b}'.format(num)[::-1]) if j == '1']


def print_row_object(r):
    print(dict(zip(r.keys(), (i for i in r))))


class Requirement(enum.Enum):
    UNSEEN = 0
    SATISFIED = 1
    UNSATISFIED = 2


class aoia:
    class Profession:
        '''Helper class to find out if a profession can use an item'''
        requirement = Requirement.UNSEEN
        prof_code = None

        def __init__(self, prof_code):
            self.prof_code = prof_code
            self.is_agent = (prof_code == 5)

        def update_state(self, skill_value, skill_id):
            if self.requirement == Requirement.SATISFIED:
                return
            if skill_value == self.prof_code or (
                    self.is_agent and skill_id == 'VisualProfession'):
                self.requirement = Requirement.SATISFIED
            else:
                self.requirement = Requirement.UNSATISFIED

        def invalid(self):
            return self.requirement == Requirement.UNSATISFIED

    def get_connection(self, db):
        con = sqlite3.connect(db)
        con.row_factory = sqlite3.Row
        self.con = con

    def get_config(self, filename='ao.yaml'):
        with open(filename) as f:
            self.config = yaml.safe_load(f)
            self.config['slot']['Spirit'] = self.config['slot']['Implant']

            self.side = self.config['sides'][self.config['config']['side']]
            self.prof = get_key(self.config['profs'],
                                self.config['config']['prof'])

    def __init__(self, con=None, db='aoitems.db'):
        self.get_config()
        if con is not None:
            self.con = con
        else:
            self.get_connection(db=db)

    def get_by_name(self, name='Eye of the Evening Star'):
        sql = 'SELECT aoid, name, ql AS ql FROM tblAOItems WHERE name LIKE (?)'
        elements = []
        with self.con:
            cur = self.con.execute(sql, [name])
            for r in cur.fetchall():
                aoid, name, ql = r['aoid'], r['name'], r['ql']
                elements.append((ql, name, aoid))
        return sorted(elements)

    def prune_name(self, name):
        if name is None:
            return ''
        elif 'implant' in name.lower():
            return name.split(':')[0]
        return name

    def get_slot_name(self, slot, slot_type):
        if (slot & 1) > 0:
            # I have no idea what this bit marker means, but I remove it
            slot ^= 1
        slots = []
        for bit in get_nonzero_bit_indices(slot):
            if slot_type == 'Weapon':
                bit %= 15
            else:
                bit -= 1
            slots.append(self.config['slot'][slot_type][bit])
        return slots

    def get_flags(self, flags=-2080374239):
        r = []
        if flags < 0:
            flags = (-flags) ^ (2**32-1)

        for flag in get_nonzero_bit_indices(flags):
            if flag in self.config['flags']:
                r.append(self.config['flags'][flag])
            else:
                print(f'unknown flag {flag}')
                r.append(f'unknown flag {flag}')
        return r

    def can_use(self, aoid):
        '''Runs tests on itemid according to criteria defined in the config'''

        with self.con:
            cur = self.con.execute(
                ("SELECT * FROM tblAOItemRequirements as R "
                 "INNER JOIN tblAOItems as I ON R.aoid = I.aoid "
                 "WHERE R.aoid=(?)"), (aoid, ))
            profession = self.Profession(self.prof)
            for r in cur.fetchall():
                skill_id = self.config['skills'].get(r['attribute'])
                skill_value = r['value']

                if skill_id == 'GM':
                    return False

                if skill_id == 'Side':
                    # operator 24 is !=
                    if r['operator'] == 24 and skill_value == self.side:
                        return False
                    # operator 0 is ==
                    elif r['operator'] == 0 and skill_value != self.side:
                        return False
                    elif r['operator'] not in (0, 24):
                        print('debug this')
                        print_row_object(r)

                if skill_id in ('Profession', 'VisualProfession'):
                    profession.update_state(skill_value, skill_id)

                for flag in self.get_flags(r['flags']):
                    if (flag == 'NoDrop'
                            and not self.config['config']['nodrop']):
                        return False
            if profession.invalid():
                return False

        # It passed all tests earlier
        return True

    def what_buffs(self):
        di = {}
        sql = '''
            SELECT E.aoid, I.name, I.ql, value2, I.islot, I.type AS itype
            FROM tblAOItemEffects AS E
                LEFT JOIN tblAOItems AS I ON E.aoid = I.aoid
                LEFT JOIN tblAONanos AS N ON E.aoid = N.aoid
            WHERE value1 = (?) and value2 > 0
                AND I.name not like "%TESTLIVE%"
            ORDER BY value2 DESC'''

        with self.con:
            cur = self.con.execute(sql, (
                get_key(self.config['skills'],
                        self.config['config']['stat_name']),))
            for r in cur.fetchall():
                if self.can_use(r['aoid']):
                    name = self.prune_name(r['name'])
                    ql = '(ql {})'.format(r['ql'])
                    slots = self.get_slot_name(r['islot'], r['itype'])
                    for slot in slots:
                        key = (r['itype'], slot)
                        if key not in di:
                            di[key] = set()
                        di[key].add((int(r['value2']), ql, name))

        return di

    def get_item(self, name=None, itemid=None, reqs=False, whatbuffs=None):
        if name is not None:
            li = self.get_by_name(name=name)
            for i in li:
                ql, name, itemid2 = i

            if reqs and len(li) > 0:
                ql, name, _ = li[-1]
                print('Fetching requirements for {} (ql {})'.format(name, ql))
                itemid = (li[-1][2])

        if itemid is not None:
            req = self.get_req(item=(itemid,))
            print('\n'.join(req))

        if whatbuffs is not None:
            self.what_buffs(whatbuffs)


def main(printing=True):
    ia = aoia()
    di = ia.what_buffs()
    if printing:
        print("Running with the following config")
        print(yaml.dump(ia.config['config']))

        old_type = ''

        for (itype, slot), li in sorted(di.items()):
            if old_type != itype:
                print('\n\n' + '#' * 5, itype, '#' * 7)
                old_type = itype

            print(f'\n# {slot} #')
            for i in sorted(li)[-5:]:
                print(i[0], i[1], i[2])
    return di, ia


if __name__ == '__main__':
    di, ia = main()
