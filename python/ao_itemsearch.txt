Running with the following config
prof: Soldier
side: clan
stat_name: Comp. Liter



##### Armor #######

# back #
15 (ql 100) Cloak of the Reanimated Jester (1/5)
16 (ql 100) Cloak of the Reanimated Jester (2/5)
17 (ql 100) Cloak of the Reanimated Jester (3/5)
18 (ql 100) Cloak of the Reanimated Jester (4/5)
20 (ql 100) Cloak of the Reanimated Jester (5/5)

# chest #
2 (ql 200) Dust Brigade Parasite Breastplate
5 (ql 1) Yellow Tank Top of Sirillion
12 (ql 250) Enhanced Kegern's Molybdenum Plate Vest
14 (ql 1) Reinforced Bau Cyber Body Armor
14 (ql 1) Reinforced Bau Cyber Female Body Armor

# feet #
2 (ql 200) Dust Brigade Parasite Boots
12 (ql 250) Enhanced Kegern's Molybdenum Plate Boots
14 (ql 1) Reinforced Bau Cyber Armor Boots
20 (ql 1) Boots of Gridspace Distortion
25 (ql 250) Cancer's Silver Boots of the Autodidact

# hands #
30 (ql 250) Dustbrigade Operative Gloves
30 (ql 300) Gauntlets of Deformation
40 (ql 250) Strong Mittens of the Sagittarius
45 (ql 300) Awakened Gauntlets of Deformation
50 (ql 200) Enhanced Dustbrigade Chemist Gloves

# head #
10 (ql 300) Shaolin's Harmonic Circlet
12 (ql 250) Enhanced Kegern's Molybdenum Plate Helmet
14 (ql 1) Reinforced Bau Cyber Armor Helmet
15 (ql 200) Jones Energized Carbonan Helmet
15 (ql 200) Loren's Helmet of Azure Reveries

# left arm #
1 (ql 1) Dust Brigade Parasite Sleeve
2 (ql 200) Dust Brigade Parasite Sleeve
12 (ql 250) Enhanced Kegern's Molybdenum Plate Sleeves
14 (ql 1) Reinforced Bau Cyber Armor Sleeves

# left finger #
1 (ql 100) Spirit Ring of Self-Education
2 (ql 200) Silly Ring
3 (ql 150) Spirit Ring of Self-Education
4 (ql 200) Ring of Presence
12 (ql 200) Soft Ring with Fluff

# left shoulder #
1 (ql 1) Frontline Surveyor Shoulderguard
2 (ql 25) Frontline Surveyor Shoulderguard
10 (ql 150) Repair Coordination Assistant

# left wrist #
6 (ql 200) Specialized Dustbrigade Vambrace - Nano Breed
10 (ql 250) First Creation of the Sagittarius
20 (ql 250) Gemini's Double Band of Linked Information

# legs #
2 (ql 200) Dust Brigade Parasite Legwear
10 (ql 250) Urbane Pants of Libra
12 (ql 250) Enhanced Kegern's Molybdenum Plate Pants
14 (ql 1) Reinforced Bau Cyber Armor Pants
25 (ql 300) Control Unit Crepuscule Pants

# neck #
250 (ql 1) Clan Merits - Paragon
250 (ql 1) Clan Merits - Xan Combat Paragon
250 (ql 1) Clan Merits - Xan Defense Paragon
275 (ql 1) Clan Merits - Awakened Combat Paragon
275 (ql 1) Clan Merits - Awakened Defense Paragon

# right arm #
1 (ql 1) Dust Brigade Parasite Sleeve
2 (ql 200) Dust Brigade Parasite Sleeve
12 (ql 250) Enhanced Kegern's Molybdenum Plate Sleeves
14 (ql 1) Reinforced Bau Cyber Armor Sleeves

# right finger #
3 (ql 150) Spirit Ring of Self-Education
4 (ql 200) Ring of Presence
12 (ql 200) Soft Ring with Fluff
15 (ql 250) Superior Ring of the Nucleus Basalis
25 (ql 250) The Bull's Ring of Literacy

# right shoulder #
1 (ql 1) Frontline Surveyor Shoulderguard
2 (ql 25) Frontline Surveyor Shoulderguard
3 (ql 300) Overheated Shoulderpad
10 (ql 150) Repair Coordination Assistant

# right wrist #
6 (ql 200) Specialized Dustbrigade Vambrace - Nano Breed
10 (ql 250) First Creation of the Sagittarius
20 (ql 250) Gemini's Double Band of Linked Information


##### Implant #######

# eyes #
82 (ql 300) Uber imp!
83 (ql 300) Intelligent Ocular Symbiant, Artillery Unit Aban
85 (ql 300) Eye Implant
87 (ql 300) Xan Ocular Symbiant, Artillery Unit
87 (ql 300) Xan Ocular Symbiant, Artillery Unit Alpha

# head #
135 (ql 290) Conscious Brain Symbiant, Artillery Unit Aban
137 (ql 300) Intelligent Brain Symbiant, Artillery Unit Aban
141 (ql 300) Head Implant
144 (ql 300) Xan Brain Symbiant, Artillery Unit
144 (ql 300) Xan Brain Symbiant, Artillery Unit Alpha

# right hand #
53 (ql 300) Xan Right Hand Symbiant, Artillery Unit Beta
55 (ql 300) Intelligent Right Hand Symbiant, Artillery Unit Aban
57 (ql 300) Right-Hand Implant
58 (ql 300) Xan Right Hand Symbiant, Artillery Unit
58 (ql 300) Xan Right Hand Symbiant, Artillery Unit Alpha


##### Weapon #######

# deck #
2 (ql 1) Tamtor's Vorpal Rapier
20 (ql 44) Galahad Inc T70 Beyer
20 (ql 76) Galahad Inc T70 Beyer

# deck6 #
6 (ql 250) Spiritech Network Analyzer
15 (ql 150) Alien Augmentation Device - Technical Knowledge

# hud1 #
6 (ql 250) Spiritech Network Analyzer
15 (ql 150) Alien Augmentation Device - Technical Knowledge

# hud2 #
6 (ql 250) Spiritech Network Analyzer
15 (ql 150) Alien Augmentation Device - Technical Knowledge

# hud3 #
6 (ql 250) Spiritech Network Analyzer

# util1 #
6 (ql 250) Spiritech Network Analyzer

# util2 #
6 (ql 250) Spiritech Network Analyzer

# util3 #
20 (ql 299) Defender of Troy
20 (ql 44) Galahad Inc T70 Beyer
20 (ql 76) Galahad Inc T70 Beyer
40 (ql 300) Defender of Troy
40 (ql 300) Excellent Missile Launcher
