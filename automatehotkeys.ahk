#Requires AutoHotkey v2.0
#SingleInstance Force
#MaxThreadsPerHotkey 2
SetTitleMatchMode "Regex"

global toons := []
global started := 0

makeGUI()

F4::
{
	global started := !started
	if (started){
		mainLoop()
	}
}

mainLoop()
{
	state.Value := "Current State: Running"
	while (started){
		for i1, e1 in toons{
			interval := e1[1]
			if (timers[i1] + interval < A_Tickcount){
				for i2, e2 in e1[2]{
					toonName := e2[1]
					keyPressed := e2[2]
					enabled := e2[3]
					if (enabled){
						; In case you pause mid loop, don't send any more keys
						if (started){
							ControlSend keyPressed,, "i)Anarchy Online - " . toonName
						}
					}
				}
				timers[i1] := A_Tickcount
			}
		}
	}
	state.Value := "Current State: Paused.."
}

readConfig(filename, d){
	toons := []
	Loop read filename{
		t := StrSplit(A_LoopReadLine, d)
		interval := t[1]
		toon := t[2]
		key := t[3]
		enabled := t[4]
		
		found := 0
		; Look for interval in 
		for i1, e1 in toons{
			if (e1[1] == interval){
				found := i1
			}
		}
		if (found == 0){
			toons.push([interval, []])
			found := toons.length
		}
		
		toons[found][2].push([toon, key, enabled])
		
	}
	return toons
}

writeConfig(filename, d){
	s := ""
	for i1, e1 in toons{
		interval := e1[1]
		for i2, e2 in e1[2]{
			toonName := e2[1]
			keyPressed := e2[2]
			enabled := e2[3]
			s := s . interval . d . toonName . d . keyPressed . d . enabled . "`n"
		}
	}
	try FileDelete filename
	FileAppend s, filename 
	
}

makeGUI(){
	if (toons.length < 1 and FileExist("aoloop.txt")){
		global toons := readConfig("aoloop.txt", ",")
	}
	global MyGUI := Gui()
	MyGUI.SetFont("s24")
	global state := MyGUI.add("Text",, "Current State: Paused..")
	MyGUI.SetFont("s14")
	global timers := []
	global LV := MyGUI.Add("ListView", "r5 Multi Grid", ["Interval(s)", "Toon name", "Keys", "enabled"])
	LV.OnEvent("DoubleClick", LV_DoubleClick)
	LV.OnEvent("ContextMenu", ShowContextMenu)
	
	for i1, e1 in toons{
		timers.push(0)
		for i2, e2 in e1[2]{
			 LV.add("",e1[1] / 1000, e2[1], e2[2], e2[3])
		}
	}
	LV.Redraw()
	
	LV.ModifyCol(1, "Integer")

	SaveBtn := MyGUI.add("Button", "vSave x0","Save Config")	
	LoadBtn := MyGUI.add("Button", "vLoad yp","Load Config")
	AddBtn := MyGUI.add("Button", "Default yp","Add")
	DeleteBtn := MyGUI.add("Button", "Default yp","Delete")
	
	AddBtn.OnEvent("Click", AddBtn_Click)
	LoadBtn.OnEvent("Click", LoadBtn_Click)
	SaveBtn.OnEvent("Click", SaveBtn_Click)
	DeleteBtn.OnEvent("Click", Delete)
	
	MyGUI.add("Text", "x0 w100","Interval(s)")
	MyGUI.add("Text", "yp w200","Charname")
	MyGUI.add("Text", "yp w100","Keys")
	global newInterval := MyGUI.add("Edit", "x0 w100","1")
	global newToon := MyGUI.add("Edit", "yp w200","t")
	global newKey := MyGUI.add("Edit", "yp w100","k")
	MyGUI.add("Text", "x0 w700", "Use F4 to pause/unpause globally, double click to pause a single entry, right click for context menu to delete entry")
	myGui.OnEvent("Close", myGui_Close)
	MyGUI.Show
	
	return
}


AddBtn_Click(*){
	found := 0
	interval := Number(newInterval.Value) * 1000
	; Look for interval in 
	for i1, e1 in toons{
		if (e1[1] == interval){
			found := i1
		}
	}
	if (found == 0){
		toons.push([interval, []])
		timers.push(0)
		found := toons.length
	}
	
	toons[found][2].push([newToon.Value, newKey.Value, 1])
	LV.add("",interval / 1000, newToon.Value, newKey.Value, 1)
	return	
}
SaveBtn_Click(*){
	writeConfig("aoloop.txt", ",")
	return
}
LoadBtn_Click(*){
	global toons := readConfig("aoloop.txt", ",")
	while (timers.length > 0){
			timers.pop()
	}
	LV.delete()
	for i1, e1 in toons{
		timers.push(0)
		for i2, e2 in e1[2]{
			 LV.add("",e1[1] / 1000, e2[1], e2[2], e2[3])
		}
	}
	return
}

ShowContextMenu(LV, Item, IsRightClick, X, Y){
    ; Create the popup menu to be used as the context menu:
    ContextMenu := Menu()
    ContextMenu.Add("Delete", Delete)
	ContextMenu.Show(X, Y)
}
LV_DoubleClick(LV, RowNumber){
	interval := LV.GetText(RowNumber, 1) * 1000
	toon := LV.GetText(RowNumber,2)
	key := LV.GetText(RowNumber,3)
	enabled := LV.GetText(RowNumber,4)
	
	
	for i1, e1 in toons{
		if (e1[1] == interval){
			found := i1
			for i2, e2 in e1[2]{
				if (e2[1] == toon and e2[2] == key){
    				
					e1[2][i2][3] := !enabled
					LV.Modify(RowNumber, "Col4", !enabled)
					LV.Redraw()
					
				}
			}
		}
	}
}

Delete(*){
	RowNumber := 0  
	Loop{
		; Get next row number.
		RowNumber := LV.GetNext(RowNumber)  
		if not RowNumber 
			; No more lines
			break
	
		interval := LV.GetText(RowNumber, 1) * 1000
		toon := LV.GetText(RowNumber,2)
		key := LV.GetText(RowNumber,3)
		enabled := LV.GetText(RowNumber,4)
		
		for i1, e1 in toons{
			if (e1[1] == interval){
				found := i1
				for i2, e2 in e1[2]{
					if (e2[1] == toon and e2[2] == key){
						e1[2].removeat(i2)
						LV.Delete(RowNumber)
					}
				}
			}
		}
	}
}


myGui_Close(thisGui) {
    ExitApp
}
